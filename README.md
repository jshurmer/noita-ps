Noita Pwsh Module
-----------------

This is a Powershell Module for saving your noita runs.

## Install

I have not published this module yet, so install it from source:

```
git clone <this-repo>
Import-Module ./noita-ps
```

**Note:** you probably want to import the module in your powershell profile

## Usage

Saving a run

```
# save a game
Save-Noita

# save a game with a name
Save-Noita -Name "long_run"

# the normal cmdlet things work as expected
Save-Noita -Confirm
Get-Help Save-Noita
```


Restoring a run

```
# List available runs
Restore-Noita -List

# Restore the default saved run
Restore-Noita

# Restore a named save
Restore-Noita -Name "long_run"

# the normal cmdlet things work as expected
Restore-Noita -WhatIf
Get-Help Restore-Noita
```


