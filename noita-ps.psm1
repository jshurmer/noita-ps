$NoitaPSSavesDir = "$env:USERPROFILE\AppData\LocalLow\Nolla_Games_Noita"
$NoitaPSDefaultSave = "saved_game"
$NoitaPSBackupSave = "backup"
$NoitaPSSaveFiles = "world", "player.xml", "session_numbers.salakieli", "world_state.xml"

<#
.Synopsis
    Save your noita run

.Description
    Save-Noita saves your current noita run. Use Restore-Noita to restore it

.Example
    Save-Noita

.Example
    Save-Noita -Name "long_run"
#>
function Save-Noita {
    [CmdletBinding(
        SupportsShouldProcess=$True,
        ConfirmImpact='Low'
    )]
    Param(
        [String]$Name = $NoitaPSDefaultSave,
        [Switch]$Force
    )

    if ($Force) {
        $ConfirmPreference = 'None'
    }

    if ($PSCmdlet.ShouldProcess($Name)) {

        # Confirm before overwriting an existing save
        if (
          @($NoitaPSDefaultSave, $NoitaPSBackupSave).contains($Name) -or 
          -not (Test-Path "$NoitaPSSavesDir\$Name.zip") -or
          $Force -or $PSCmdlet.ShouldContinue($Name, "Overwrite Previous Save")
        ) {
            Write-Host "Saving current run as $Name"
            Compress-Archive -Confirm:$false -Force -Path ($NoitaPSSaveFiles | % {Join-Path $NoitaPSSavesDir "save00" $_}) -DestinationPath "$NoitaPSSavesDir\$Name.zip" -ErrorAction 'Ignore'

        }
        Write-Host "✔ $Name saved"
        Write-Verbose "$NoitaPSSavesDir\$Name.zip"
    }
}

<#
.Synopsis
    Restore a previous Noita save

.Description
    Restore-Niota restores a saved Noita run. Use Save-Noita to save a run.

.Example
    # Display available saved runs
    Restore-Noita -List

.Example
    Restore-Noita

.Example
    Restore-Noita -Name "long_run"
#>
function Restore-Noita {
    [CmdletBinding(
        SupportsShouldProcess=$True,
        ConfirmImpact='High'
    )]
    Param(
        [String]$Name = $NoitaPSDefaultSave,
        [Switch]$List,
        [Switch]$Force
    )

    if ($List) {
        ls "$NoitaPSSavesDir/*.zip"
        return
    }

    if ($Force) {
        $ConfirmPreference = 'None'
    }

    if (-not $(Test-Path "$NoitaPSSavesDir\$Name.zip")) {
        Write-Error "No save $Name found"
        return
    }

    if ($Name -ne $NoitaPSBackupSave -and -not $Force) {
        Write-Host "Backuping up current run as $NoitaPSBackupSave"
        Save-Noita -Name $NoitaPSBackupSave
    }

    if ($PSCmdlet.ShouldProcess($Name, "Overwrite existing run with save file")) {
        Write-Host "Restoring saved run $Name"
        Expand-Archive -Force -Path "$NoitaPSSavesDir\$Name.zip" -DestinationPath "$NoitaPSSavesDir\save00"
        Write-Host "✔ $Name restored"
    }

}

